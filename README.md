## Term Revision

This module allows you to create revisions for a taxonomy term.

You can view all created revisions in tabular form, view the
contents of a particular revision, delete a revision and also
revert to a previous revision.

### Features

* View created revisions on taxonomy term
* View a specific revision
* Delete a revision
* Revert to a previous revision
* Requirements
* No special requirements.

### Install/Usage

Install module via composer.

Just enable module and clear cache. Taxonomy terms will now
have revision support.
